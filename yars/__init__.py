"""
YARS - yet another RSS downloader

    by W-Mark Kubacki; wmark@hurrikane.de
    licensed under the terms of the RPL for non-commercial usage
"""

try:
	import anzu.options
	anzu.options.enable_pretty_logging()
	from anzu.options import logging
except:
	import logging

from model import WantedItem, SourceManager

__all__ = ['YARS']
__author__ = "W-Mark Kubacki; wmark@hurrikane.de"
__version__ = "1.1"

class YARS(object):

	def __init__(self, redis, prefix, verbose=False):
		self.db = redis
		self.prefix = prefix
		self.verbose = verbose

	def get_desired_items_from_feeds(self, remember_end_item=False, case_sensitive=False):
		if not self._check_prerequisities_for_run():
			return False
		feeds = SourceManager(self.db, self.prefix)
		needles = WantedItem(self.db, self.prefix)
		result = []
		for haystack in feeds:
			logging.debug('On feed "%s"...', haystack.caption)
			p = haystack.get(needles, remember_end_item, case_sensitive)
			logging.info('Feed "%s" yielded %d matches.', haystack.caption, len(p))
			result += p
		logging.info('Run is complete. Result consists of %d items.', len(result))
		return result
	run = get_desired_items_from_feeds

	def _check_prerequisities_for_run(self):
		if SourceManager.count(self.db, self.prefix) <= 0:
			print("No feeds to download from. Exiting.")
			return False
		if WantedItem.count(self.db, self.prefix) <= 0:
			print("No 'wanted' list for matching, or list is empty. Exiting.")
			return False
		return True

	def cleansweep(self):
		for k in self.db.keys(self.prefix + "*"):
			self.db.delete(k)
		return True

	@classmethod
	def display_items(cls, desired_items, field):
		for item in desired_items:
			print item[field]
