"""
YARS - yet another RSS downloader

    by W-Mark Kubacki; wmark@hurrikane.de
    licensed under the terms of the RPL for non-commercial usage
"""

try:
	import cPickle as pickle
except:
	import pickle

try:
	import anzu.options
	anzu.options.enable_pretty_logging()
	from anzu.options import logging
except:
	import logging

import feedparser

from util import string_contains_all, string_contains_any

__all__ = ['WantedItem', 'RSSFeed', 'SourceManager',
	   ]

class WantedItem(object):
	"""Representation of a sophisticated search term.

	Used in YARS to match against feed entries.
	"""

	def __init__(self, redis, prefix, caption=None, search_words=None, exclude_words=set(), id=None):
		self.db = redis
		self.prefix = prefix
		self.id = id
		self.caption = caption
		self.search_words = search_words
		self.exclude_words = exclude_words
		self.search_words_lower = [w.lower() for w in search_words] if search_words else []
		self.exclude_words_lower = [w.lower() for w in exclude_words] if exclude_words else []

	def __get_id(self):
		"""Guaranteed to return a valid unique id.

		If this items has none, a new is reserved.
		"""
		if self.id:
			return self.id
		else:
			self.id = str(self.db.incr(self.prefix + '/wanted/next_key'))
			return self.id

	def sync(self):
		"""Creates or updates the corresponding entry in database.
		"""
		id = self.__get_id()
		self.db.set(self.prefix + '/wanted/' + id + "/caption", self.caption)
		for word in self.search_words:
			self.db.sadd(self.prefix + '/wanted/' + id + "/search_words", word)
		for word in self.exclude_words:
			self.db.sadd(self.prefix + '/wanted/' + id + "/exclude_words", word)
		self.db.sadd(self.prefix + '/wanted', id)
		return id

	@classmethod
	def delete(cls, redis, prefix, id):
		"""Deletes the corresponding entry.
		"""
		n = str(id)
		if redis.srem(prefix + '/wanted', id):
			for k in [prefix + '/wanted/' + n + '/caption',
				  prefix + '/wanted/' + n + '/search_words',
				  prefix + '/wanted/' + n + '/exclude_words']:
				redis.delete(k)
			return True
		else:
			return False

	@classmethod
	def get_by(cls, redis, prefix, id):
		n = str(id)
		caption = redis.get(prefix + '/wanted/' + n + "/caption")
		search_words = redis.smembers(prefix + '/wanted/' + n + "/search_words")
		exclude_words = redis.smembers(prefix + '/wanted/' + n + "/exclude_words")
		return WantedItem(redis, prefix, caption, search_words, exclude_words, n)

	@classmethod
	def count(cls, redis, prefix):
		return redis.scard(prefix + '/wanted')

	def __iter__(self):
		self.wanted = list(self.db.smembers(self.prefix + '/wanted'))
		self.iter_on = 0
		return self

	def next(self):
		if len(self.wanted) >= 1 and self.iter_on < len(self.wanted):
			self.iter_on += 1
			return WantedItem.get_by(self.db, self.prefix, self.wanted[self.iter_on - 1])
		else:
			raise StopIteration

	def __str__(self):
		return "%4s | %20s | %40s | %40s" % (self.id, self.caption, self.search_words, self.exclude_words)

	def matches(self, feeditem, case_sensitive):
		if case_sensitive:
			title, desired, unwanted = feeditem['title'], self.search_words, self.exclude_words
		else: # case-insensitive, the default
			title, desired, unwanted = feeditem['title'].lower(), self.search_words_lower, self.exclude_words_lower
		return         string_contains_all(title, desired) \
		       and not string_contains_any(title, unwanted)


class FeedModel(object):
	"""Represents a feed in database."""

	def __init__(self, redis, prefix, caption=None, url=None, lastpos=0, id=None):
		self.db = redis
		self.prefix = prefix
		self.caption = caption
		self.url = url
		self.id = id

	def __get_id(self):
		"""Guaranteed to return a valid unique id.

		If this items has none, a new is reserved.
		"""
		if self.id:
			return self.id
		else:
			self.id = str(self.db.incr(self.prefix + '/feeds/next_key'))
			return self.id

	def sync(self):
		"""Creates or updates the corresponding entry in database.
		"""
		id = self.__get_id()
		self.db.mset({self.prefix + '/feed/' + id + '/url': self.url,
			      self.prefix + '/feed/' + id + '/caption': self.caption,
			      self.prefix + '/feed/' + id + '/lastpos': 0
			      })
		self.db.sadd(self.prefix + '/feeds', id)
		return id

	def reset_lastpos_mark(self):
		if self.id:
			self.db.set(self.prefix + '/feed/' + self.id + '/lastpos', 0)

	def __str__(self):
		return "%4s | %20s | %s " % (self.id, self.caption, self.url)


class FeedLogic(FeedModel):
	"""Responsible for getting latest entries from a list.

	If you implement something other on top of this, define or overwrite:
	* load_page(lastpos=None) # (If none, only fetch the first page.)
	* feed_entry_id(any_feed_entry)

	Feed-Items are dicts with at least the fields ['title', 'link', 'summary'].
	Summary is for display purposes only, "title" actually used for matching.
	"""

	def feed_entry_id(self, feed_entry):
		"""Guaranteed to get an ID for a given feed entry.

		Not a classmethod so it can be overwritten.
		"""
		if 'id' in feed_entry:
			return feed_entry['id']
		return feed_entry['link']

	def sieve_feed_entries(self, feed_entries, lastpos):
		"""Removes entry with ID 'lastpos' and every entry after that."""
		if not lastpos:
			return feed_entries
		else:
			entries = list()
			for entry in feed_entries:
				if self.feed_entry_id(entry) == lastpos:
					break
				else:
					entries.append(entry)
			return entries

	def load_and_parse(self, lastpos=None, load_from_cache=True, expiry=3600):
		key = self.prefix + '/feed/' + self.id + '/lastparsed'
		if load_from_cache:
			rss = self.db.get(key)
			if rss:
				logging.info('Feed "%s" has been found in, and loaded from cache.', self.url)
				return pickle.loads(rss)
		try:
			logging.debug('Feed "%s" has not been cached, yet. Or its entry expired. It will be fetched...', self.url)
			entries = self.load_page(lastpos)
			logging.info('Feed "%s" has successfully been retrieved.', self.url)
			self.db.set(key, pickle.dumps(entries))
			self.db.expire(key, expiry)
			return entries
		except Exception, e:
			logging.error('Fetching feed "%s" failed with this exception: %s', self.url, e)
			raise

	def get_latest_items(self, remember_end_item=True):
		# Operates under the assumption that the most recent item is on top ([0]).
		lastpos_key = self.prefix + "/feed/" + self.id + "/lastpos"
		lastpos = self.db.get(lastpos_key)
		if not lastpos: logging.debug('Feed "%s" has no last position marker.', self.url)
		try:
			f = self.load_and_parse(lastpos)
			logging.info('Loading and parsing of "%s" succeeded.', self.url)
		except:
			logging.debug('Something went wrong at loading and parsing of "%s". Result: empty.', self.url)
			return []
		if remember_end_item:
			if not lastpos or (f is not None and len(f) > 0 and lastpos != self.feed_entry_id(f[0]) ):
				self.db.set(lastpos_key, self.feed_entry_id(f[0]))
		logging.debug('"%s" has %d feed-entries in total.', self.url, len(f))
		latest = self.sieve_feed_entries(f, lastpos)
		logging.info('"%s" has %d new feed-entries.', self.url, len(latest))
		return latest

	def get(self, wanted_items, remember_end_item=True, case_sensitive=False):
		"""Gets the wanted items from the feed, if present."""
		items_to_be_considered = self.get_latest_items(remember_end_item=remember_end_item)
		logging.debug("%d new feed-items will be matched against.", len(items_to_be_considered))
		results = []
		for item in items_to_be_considered:
			for needle in wanted_items:
				if needle.matches(item, case_sensitive):
					logging.debug('WantedItem "%s" matched feed-item "%s". Will be added to result.', needle.caption, item)
					results.append(item)
					break
		return results


class RSSFeed(FeedLogic):
	"""Representation of a RSS or Atom Feed."""

	def load_page(self, lastpos=None):
		f = feedparser.parse(self.url)
		if 'bozo_exception' in f:
			try:
				error = f['bozo_exception'].reason
			except:
				error = str(f['bozo_exception'])
#			raise Exception(error)
		return f['entries']


class SourceManager(object):
	"""Proxy to and factory of all sources, such as RSSFeeds.

	Use this whenever you need to get any sources.
	"""

	def __init__(self, redis, prefix):
		self.db = redis
		self.prefix = prefix

	@classmethod
	def add_feed(cls, redis, prefix, caption, url):
		new_feed = RSSFeed(redis, prefix, caption, url)
		return not not new_feed.sync()

	@classmethod
	def delete_feed(cls, redis, prefix, id):
		n = str(id)
		if redis.srem(prefix + '/feeds', id):
			for k in [prefix + '/feed/' + n + '/url',
				  prefix + '/feed/' + n + '/caption',
				  prefix + '/feed/' + n + '/lastparsed',
				  prefix + '/feed/' + n + '/lastpos']:
				redis.delete(k)
			return True
		else:
			return False

	@classmethod
	def get_by(cls, redis, prefix, id):
		n = str(id)
		caption, url, lastpos = redis.mget([prefix + '/feed/' + n + '/caption',
						    prefix + '/feed/' + n + '/url',
						    prefix + '/feed/' + n + '/lastpos'])
		return RSSFeed(redis, prefix, caption, url, lastpos, n)

	@classmethod
	def count(cls, redis, prefix):
		return redis.scard(prefix + '/feeds')

	def __iter__(self):
		self.feeds = list(self.db.smembers(self.prefix + '/feeds'))
		self.iter_on = 0
		return self

	def next(self):
		if len(self.feeds) >= 1 and self.iter_on < len(self.feeds):
			self.iter_on += 1
			return SourceManager.get_by(self.db, self.prefix, self.feeds[self.iter_on - 1])
		else:
			raise StopIteration

