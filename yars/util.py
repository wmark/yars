"""
YARS - yet another RSS downloader

    by W-Mark Kubacki; wmark@hurrikane.de
    licensed under the terms of the RPL for non-commercial usage
"""

__all__ = ['merge', 'multi_merge', 'string_contains_all', 'string_contains_any',
	   ]

def merge(keyf, left, right):
	"""Merges two sorted lists."""
	result = []
	i, j = 0, 0
	while i < len(left) and j < len(right):
		if keyf(left[i]) <= keyf(right[j]):
			result.append(left[i])
			i += 1
		else:
			result.append(right[j])
			j += 1
	result += left[i:]
	result += right[j:]
	return result

def multi_merge(keyf, items_list):
	"""Merges an arbitrary number of sorted lists."""
	result, remainder = items_list[0], items_list[1:]
	for l in remainder:
		result = merge(keyf, result, l)
	return result

def string_contains_all(haystack, needles):
	for needle in needles:
		if not needle in haystack:
			return False
	return True

def string_contains_any(haystack, needles):
	for needle in needles:
		if needle in haystack:
			return True
	return False
